<?php
// Check for empty fields
if(empty($_POST['name'])        ||
   empty($_POST['email'])       ||
   empty($_POST['message'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
{
    echo "No arguments Provided!";
    return false;
}
	
$name = strip_tags(htmlspecialchars($_POST['name']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));
$message = strip_tags(htmlspecialchars($_POST['message']));
	
// Create the email and send the message
$to = 'megver83@parabola.nu'; // Add your email address in between the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
$email_subject = "[megver83.ga] Formulario de contacto de $name";
$email_body = "Haz recibido un nuevo mensaje de tu formulario de contacto de tu sitio web.\n\n"."Aqui estan los detalles:\n\nNombre: $name\n\nEmail: $email_address\n\nMensaje:\n$message";
// $headers = "From: noreply@megver83.ga\n"; // This is the email address the generated message will be from. In my case, this is set by msmtp.
$headers .= "Reply-To: $email_address";	
mail($to,$email_subject,$email_body,$headers);
return true;
