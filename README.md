## [Megver83's personal website](https://megver83.ga/)
This is the official repository for https://megver83.ga/ and related websites.

The main website is based on Freelancer, read more about the template in the megver83.ga/README.md file.

**NOTE**: Although it says that Freelancer is MIT-licensed, I changed it to GLPv3, which is the license for most of the files here. Some of them might have other free software licenses, which in the case the are compatible with the GPL, they will have GPL and its original license (it doesn't have to say explicitly in comments or similar). In the case it's not compatible, that won't apply.

### Translating
If you wish to help me by translating my website, you can open an issue in the bug tracker and provide the translated files **or** you can [send me an email](mailto:megver83@parabola.nu). I'd very thankful!

### Special acknowledgements
Thanks to [Jesús Eduardo](https://heckyel.ga) who helped with the libre [social networks logos](https://notabug.org/Heckyel/libresocial)!
